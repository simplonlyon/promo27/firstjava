package co.simplon.promo27;

import java.util.ArrayList;
import java.util.List;

public class MainIntroduction {


     /**
     * La fonction public static void main est le point d'entrée d'un programme Java, 
     * lorsqu'on exécute celui ci, il cherchera cette fonction pour la lancer. Techniquement
     * un programme peut avoir plusieurs main mais alors il faudra choisir à l'exécution lequel
     * on souhaite lancer (c'est assez rare)
     */
    @SuppressWarnings("all") //Évitez d'utiliser ça, je l'ai mis pour qu'il enlève les warnings dans cette fonction car il y a beaucoup de variable inutilisées
    public static void main(String[] args) {
        /**
         * Pour déclarer une variable en Java, pas de mot clef let/const/var, à la place
         * on doit indiquer le typage de la variable (tout est obligatoirement typé en Java)
         * suivi de son nom et éventuellement lui assigner une donnée
         */
        String message = "coucou";

        /**
         * Il existe plusieurs types primitifs (les typage les plus simple possible
         * qui ne possèdent pas de méthode) qui sont le char (une seule lettre) délimité
         * par des simple quote, les nombres entier byte/short/int/long (leur taille
         * varie mais en gros on utilise le int par défaut et si on est obligé, le long),
         * les nombres décimaux float et double (pareil on peut dire qu'on utilise double
         * par défaut) et le boolean
         */
        char lettre = 'c';

        byte tresPetitNombreEntier = 123;
        short petitNombreEntier = 13400;
        int nombreEntier = 1250000000;
        long grosNombreEntier = 1000000000000000000l;

        float nombreDecimal = 2.3f;
        double grosNombreDecimal = 2.3333333333333333333333;

        boolean variableBooleenne = true;

        String[] tab = new String[6];
        tab[0] = "bloup";
        tab[1] = "bliip";
        

        List<String> maListe = new ArrayList<>(List.of("peut", "être"));

        maListe.add("salut"); // en JS : maListe.push("salut")
        maListe.size(); // En JS : maListe.length
        maListe.get(0); // en JS : maListe[0]

        for (String item : maListe) {//En JS : for(let item of maListe) {}
            System.out.println(item);
        }

        //équivalent de console.log()
        System.out.println("Hello world!");

        System.out.println(message);
        //Les if-else ont la même syntaxe qu'en JS
        if (variableBooleenne) {
            System.out.println("quelque chose");
        } else {
            System.out.println("autre chose");
        }

        if (message == "coucou") {
            System.out.println("le message est le même");
        }
        //Les for classique ont également la même syntaxe à l'exception du déclaration du compteur où on indique sont type, un int
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }

        //Exercice leet converter
        String sentence = "salut tout le monde...";

        for (int i = 0; i < sentence.length(); i++) {
            char letter = sentence.charAt(i);
            if (letter == ' ') {
                System.out.println();
            } else if (letter == 'a') {
                System.out.print('@');
            } else if (letter == 'l') {
                System.out.print('1');
            } else if (letter == 'e') {
                System.out.print('3');
            } else if (letter == 'o') {
                System.out.print('0');
            } else if (letter == 's') {
                System.out.print('5');
            } else {
                System.out.print(letter);
            }
        }
        if(sentence.endsWith("...")) {
            System.out.println(" uwu");
        }

    }
}
