package co.simplon.promo27.exopoo;

public class Food {

    private String label;
    private int nurrishment;

    public Food(String label, int nurrishment) {
        this.label = label;
        this.nurrishment = nurrishment;
    }
    
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public int getNurrishment() {
        return nurrishment;
    }
    public void setNurrishment(int nurrishment) {
        this.nurrishment = nurrishment;
    }
}
