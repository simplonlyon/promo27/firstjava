package co.simplon.promo27.cli;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PromoTest {

    @Test
    void shouldAddStudent() {

        Promo instance = new Promo();
        
        assertEquals("The promo contains 1 students", 
                    instance.addStudent("test"));
        assertEquals("The promo contains 2 students", 
                    instance.addStudent("test"));
    }

    @Test
    void shouldNotAddEmptyName() {
        Promo instance = new Promo();

        assertEquals("Empty name is forbidden", instance.addStudent(""));
    }

    @Test
    void shouldNotAddStudentIfFull() {

        Promo instance = new Promo(1);
        
        assertFalse(instance.isFull());
        instance.addStudent("test");
        assertEquals("Promo is full", 
                    instance.addStudent("test"));
        assertTrue(instance.isFull());

    }

    @Test
    void shouldDisplayStudentList() {
        Promo instance = new Promo();

        instance.addStudent("Test1");
        instance.addStudent("Test2");
        instance.addStudent("Test3");

        assertEquals("Test1\nTest2\nTest3\n", instance.display());

        //Manière alternative avec les """ """, ça fait la même chose
        assertEquals("""
        Test1
        Test2
        Test3
        """, instance.display());
    }
}
