package co.simplon.promo27.todo;

import java.util.ArrayList;
import java.util.List;

public class TodoMain {

    public static void main(String[] args) {
        Task todo = new Task("Buy a sandouiche");
        Task todo2 = new Task("Go to the doctor because of my sandouiche");

        todo.display();
        todo2.toggle();
        todo2.display();


        List<Task> todoList = new ArrayList<>();
        todoList.add(todo);
        todoList.add(todo2);


        for (Task task : todoList) { //équivalent à for(let task of todoList) {} en JS
            task.display();
        }

    }
}
