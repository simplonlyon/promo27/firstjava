package co.simplon.promo27;
/**
 * Une classe est un modèle/un moule permettant de créer des objets qui auront la
 * même structure que celle définie dans la classe.
 * Ici, on dit qu'une "Person" sera un objet avec un firstName et un age, on appel
 * ça des propriétés (c'est une variable mais qui fait partie d'une classe/d'un objet)
 * Pour le moment la classe définie juste la structure, c'est au moment où l'on créera
 * une ou des instances de celle ci qu'on pourra indiquer des valeurs qui seront assignées
 * aux propriétés (voir dans le Main.java)
 */
public class Person {
    public String firstName;
    public Integer age;
    
    /**
     * On définit ici un constructeur pour la classe Person, cette méthode sera appelée
     * à chaque fois qu'on fera une instance de Person. Ici on indique que pour faire
     * une instance de Person, on attend une String et un Integer qu'on assignera aux
     * propriétés de l'instance
     */
    public Person(String firstName, Integer age) {
        this.firstName = firstName;
        this.age = age;
    }


    public void bonjour() {
        System.out.println("Bonjour, je m'appelle "+firstName+" mon prenom contient "+firstName.length()+" lettres");
    }

}