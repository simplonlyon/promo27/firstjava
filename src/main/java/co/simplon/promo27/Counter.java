package co.simplon.promo27;

public class Counter {
    public int value;

    public Counter() {
        value = 0;
    }

    public Counter(int value) {
        this.value = value;
    }

    public void increment() {
        value++;
        //ou bien, comme ça c'est pareil
        //this.value++
    }

    public void decrement() {
        if(value > 0) {
            value--;
        }
    }
}