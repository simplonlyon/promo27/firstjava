package co.simplon.promo27.todo;

public class Task {
    private String label;
    private boolean finished = false;

    public Task(String label) {
        this.label = label;
    }

    public void toggle() {
        // if(finished) {
        //     finished = false;
        // } else {
        //     finished = true;
        // }

        finished = !finished;
    }

    public void display() {
        if(finished) {
            System.out.println("☒ "+label);
        } else {
            System.out.println("☐ "+label);
        }


        // System.out.println(finished ? "☒" : "☐" +label);
    }
    
}
