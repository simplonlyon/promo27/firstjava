package co.simplon.promo27;

public class Main {




    public static void main(String[] args) {
        /**
         * Ici on fait 2 instances de la même classe Person, on se retrouve donc
         * avec 2 objets ayant la même structure que définie dans la classe, mais
         * chaque objet possède ses propres valeurs qui ne sont pas liées. C'est à
         * dire que si on renomme la première personne, la seconde personne ne sera
         * pas renommée
         */
        Person instanceDePerson = new Person("Prénom", 10);
        Person autreInstance = new Person("Autre", 54);
        //Pour accéder aux propriétés d'une instance, on utilise le . comme on a l'habitude
        autreInstance.firstName = "Toto";

        instanceDePerson.bonjour();
        autreInstance.bonjour();
        //Ici on concatène les prénoms des deux instances
        System.out.println(autreInstance.firstName+" "+instanceDePerson.firstName);


        Counter counter1 = new Counter(32);
        Counter counter2 = new Counter();

        counter1.increment();
        counter1.increment();
        System.out.println(counter1.value);

        counter2.decrement();
        counter2.decrement();

        System.out.println(counter2.value);

    }












   

}
