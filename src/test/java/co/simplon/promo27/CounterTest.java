package co.simplon.promo27;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/**
 * Un test consiste 
 */
public class CounterTest {
    
    @Test
    public void trueShouldBeTrue() {
        assertTrue(true);
    }

    @Test
    public void shouldIncrementValue() {
        Counter instance = new Counter();
        assertSame(0, instance.value);

        instance.increment();
        assertSame(1, instance.value);
    }

    @Test
    public void shouldDecrementValue() {
        Counter instance = new Counter(10);
        assertSame(10, instance.value);

        instance.decrement();
        assertSame(9, instance.value);
    }

    @Test
    public void shouldNotDecrementBelowZero() {
        Counter instance = new Counter();
    

        instance.decrement();
        assertSame(0, instance.value);
    }

}
