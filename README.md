# First Java
Premier projet Java créé avec Maven

## Création d'un projet maven simple via VsCode
1. Ctrl + Shift + P => Create Java Project
2. Choix du build tool : maven
3. No archetype (on pourrait en sélectionner un, c'est des genre de template de projet, mais ceux proposés par défaut sont généralement un peu obsolète, mais on peut en faire nous même)
4. Group Id : Le package représentant le nom de l'entreprise et permettant de regrouper des projets ensemble. Par exemple nous nos projets auront `co.simplon.promo27`
5. Artifact Id : Le nom du projet
6. Modifier la version de java dans le pom.xml (maven.compiler.source et maven.compiler.target) pour passer en 21
7. Créer un .gitignore et y ajouter le dossier target

## Structure du projet
* **src/main/java** Dossier dans lequel le code java sera placé
* **src/main/resources** Dossier dans lequel on placera les fichier de configuration
* **src/test/java** Dossier dans lequel on mettra les tests de notre code java
* **src/test/resources** Dossier dans lequel on placera les fichier de configuration pour les tests
* **pom.xml** L'équivalent du package.json des projet JS, un fichier xml qui contient les informations sur le projet (nom du projet, de l'entreprise, version du projet, de java) ainsi que la liste des dépendances de celui ci
* **target** Dossier dans lequel sera placé le résultat de la compilation du code, on y touche pas, on le gitignore, son contenu est regénéré à chaque modification du code


## Exercices

### 133t 5p3@k converter (leet speak converter)
1. Tout en bas de la fonction main, créer une variable sentence de type string avec une phrase à l'intérieur
2. Faire une boucle for classique qui va faire autant de tour qu'il y a de caractère dans la phrase (on peut récupérer ça avec .length() comme en JS). À chaque tour de boucle, récupérer et afficher le caractère actuelle en utilisant la méthode charAt(...) de la sentence
3. Plutôt que faire un println, faire un print (comme ça, ça affiche sur une même ligne) et faire un println vide si le caractère est un espace (via une condition donc)
4. Faire des conditions pour faire que si le caractère est un a on le remplace par @, si c'est un e on le remplace par 3, si c'est un l par 1, si c'est un s par 5 et si c'est o par 0 (zéro)
Bonus: si la phrase se termine par ... on rajoute uwu à la fin, si elle se termine par ! on rajoute >w< et si elle se termine par ? on rajoute é_è

###  Counter class ([fichier](src/main/java/co/simplon/promo27/Counter.java))
1. Créer une classe Counter dans le package par défaut (co.simplon.promo27)
2. Dans cette classe, définir une propriété de type int qu'on va appeler value
3. Créer un constructeur qui assignera 0 à la value
4. Créer une méthode increment qui ne renvoie rien (void) et qui n'attend aucun argument, qui va incrémenter la value de 1
5. Créer une autre méthode decrement qui va baisser la valeur de 1
6. Dans le main, créer une instance de counter et lancer ses méthode puis afficher sa value
7. Faire une deuxième instance et voir si le fait d'incrémenter l'un a un effet sur l'autre

**Bonus:** Faire une méthode reset qui remet à zéro et modifier la méthode decrement pour faire qu'on ne puisse pas aller en dessous de zéro

### TodoList mais juste la task ([fichier](src/main/java/co/simplon/promo27/todo/Task.java))
1. Créer une classe Task dans le package co.simplon.promo27.todo
2. Dans cette classe, créer 2 propriété label en String et finished en boolean, les 2 private
3. Créer un constructeur qui attendra un label en argument et qui mettra finished à false par défaut
4. Créer une méthode toggle() qui ne renvoit rien et qui passera finished à true s'il est false ou finished à false s'il est true
5. Créer une autre méthode display() qui ne renvoit rien et qui fera un sysout de `☒ le label` ou de `☐ le label` selon si finished est true ou false
6. Créer une classe MainTodo (peu importe dans quel package) avec un public static void main dedans et y faire une ou deux instance de Task et tester les 2 méthodes

### Un Scanner pour faire une liste de choses ([fichier](https://gitlab.com/simplonlyon/promo27/firstjava/-/commit/4c9722c57b06c054c6b23cbf07f1f8807062143b))
1. Dans le main de la classe ListMain, créer une variable names qui sera une List de String (initialisée vide)
2. Dans le while du scanner, récupérer l'inputUser et l'ajouter à la liste de names (avec un .add() )
3. Après chaque add, afficher "Number of names : " suivi de la size de la liste, puis faire un foreach sur la liste pour afficher chaque nom précédé de * (ou - ou _ on s'en fout)
4. Créer une variable promoSize en int avec un nombre à l'intérieur, genre 5
5. À l'intérieur de la boucle, faire en sorte qu'il nous demande des noms si la taille de la liste est inférieure à la variable promoSize, et sinon faire le foreach puis faire un `break;` après celui ci pour stopper le while

### Classe Promo ([fichier](src/main/java/co/simplon/promo27/cli/Promo.java))
Une classe Promo
1. Créer une classe co.simplon.promo27.cli.Promo
2. Dans cette classe, mettre en propriétés (private) la liste de names qu'on a fait dans ListMain, mais on va la renommer students et la promoSize
3. Créer un constructeur permettant d'assigner une size à la promo et un constructeur vide qui met une size par défaut à 5
4. Créer une méthode addStudent(String name) qui va renvoyé une String et qui v adonc faire un add sur la liste Student mais en faisant 2 vérifications : que name n'est pas une chaîne de caractère vide et que la taille de la liste ne dépasse pas celle indiquée dans promoSize
5. Faire que cette méthode renvoie "The promo contains ... students" en mettant le nombre actuel de student si le add se fait bien, et si le if ne passe pas on renvoie "The promo is full" ou bien "Empty name forbidden" selon la validation qui n'est pas passée
6. Créer une autre méthode display() qui va renvoyer une String et qui va donc faire un foreach sur les students et pour chaque student concaténé le name du student avec un saut de ligne `"\n"` dans une String déclarée avant la boucle
7. Modifier le ListMain pour n'y garder que le Scanner, une instance de Promo et le while (on aura probablement besoin d'une méthode getCurrentSize() qui renverra la size de la liste pour pouvoir faire le while)

### Tester la Promo ([fichier](src/test/java/co/simplon/promo27/cli/PromoTest.java))
1. Créer une classe PromoTest dans le dossier de test (ou générer la, action de la source, générer)
2. Créer une méthode void shouldAddStudent et dedans faire une instance de Promo et appeler la méthode addStudent avec un nom de test dedans. Faire une assertion pour vérifier qu'on obtient bien la phrase avec le nombre de student (a priori 1) 
3. Faire une autre méthode shouldNotAddEmptyName dans laquelle on test en appelant avec une chaîne de caractères vide
4. Et une shouldNotAddIfFull dans laquelle on test que ça ajoute pas si promo pleine (on peut faire une instance avec une taille de promo différente pour l'occasion)
5. Enfin, faire un test de shouldDisplayStudentList dans laquelle on add 1-3 student et on vérifie que display renvoie la bonne chaîne de caractères

### Las creaturas ([fichier](src/main/java/co/simplon/promo27/exopoo/Creature.java))
1. Créer une classe co.simplon.promo27.exopoo.Creature avec comme propriété name en String, un type en String, un affection en int et un hunger en int, toutes privées
2. Générer un constructeur qui va attendre juste le nom et le type et assigner une hunger à 0 et une affection à 50 par défaut
3. Générer une méthode toString (au même endroit que pour générer le constructeur) avec tous les champs sélectionnés
4. Créer une classe Main dans le package exopoo et dedans faire 2-3 instances de Creature et en faire des sout
5. Faire une classe Food avec un label en String, un nurrishment en int, constructeur plein et getter/setter pour tout le monde
6. Ajouter une propriété Food food initialisée null sur la Creature
7. Créer une méthode giveFood(Food food) dans Creature pour assigner à la propriété (en vrai c'est un setter qui porte pas son nom) et qui augmente l'affection de 10
8. Créer une méthode eat() qui va faire baisser la hunger de la valeur de nurrishment de la food et passera la food à null
9. Créer une méthode share(Creature creature) qui va faire baisser la hunger de la créature actuelle (this) et de la creature en paramètre de la moitié de nurrishment et augmentera la affection des 2 de 20