package co.simplon.promo27.exopoo;

public class Creature {

    private String name;
    private String type;
    private int hunger;
    private int affection;
    private Food miammiam;

    public Creature(String name, String type) {
        this.name = name;
        this.type = type;
        this.hunger = 0;
        this.affection = 50;
        this.miammiam = null; //optionnel, par défaut c'est null en fait
    }

    public void giveFood(Food food) {
        miammiam = food;
        System.out.println(name+" accepts the "+food.getLabel()+", it makes it like you more");
        affection += 10;
    }

    public void eat() {
        hunger -= miammiam.getNurrishment();
        System.out.println(name+" is eating "+miammiam.getLabel());
        miammiam = null;
    }

    public void share(Creature creature) {
        int half = miammiam.getNurrishment()/2;
        this.hunger -= half;
        creature.hunger -= half;
        this.affection += 20;
        creature.affection += 20;
        System.out.println(name+" share its "+miammiam.getLabel()+ " with "+creature.name+". Friendship is wonderful");
        miammiam = null;
    }

    @Override
    public String toString() {
        return "Creature [name=" + name + ", type=" + type + ", hunger=" + hunger + ", affection=" + affection + "]";
    }

}
