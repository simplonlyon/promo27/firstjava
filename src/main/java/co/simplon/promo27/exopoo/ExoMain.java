package co.simplon.promo27.exopoo;

public class ExoMain {


    public static void main(String[] args) {
        Creature creature1 = new Creature("Rompiflex", "Chat");
        Creature creature2 = new Creature("Poulex", "Poulet");

        System.out.println(creature1);


        Food bruschetta = new Food("Brochette de champignon", 20);
        System.out.println(bruschetta.getLabel() + bruschetta.getNurrishment());

        creature1.giveFood(bruschetta);
        creature1.share(creature2);
    }
}
