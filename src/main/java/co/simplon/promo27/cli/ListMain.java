package co.simplon.promo27.cli;

import java.util.Scanner;

public class ListMain {
    public static void main(String[] args) {
        Promo promo27 = new Promo(4);
        
        Scanner scanner = new Scanner(System.in);
        
        while(!promo27.isFull()) {
            System.out.println("Enter a student name plz");
            String input = scanner.nextLine();
            String feedback = promo27.addStudent(input);
            System.out.println( feedback ) ;
        }

        System.out.println(promo27.display());
        
        scanner.close();
    }
}
