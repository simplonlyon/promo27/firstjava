package co.simplon.promo27.cli;

import java.util.ArrayList;
import java.util.List;

public class Promo {
    private List<String> students = new ArrayList<>();
    private int promoSize;

    public Promo() {
        this.promoSize = 5;
    }
    public Promo(int promoSize) {
        this.promoSize = promoSize;
    }
    /**
     * Méthode qui ajoute un student dans la liste si la promo n'est pas pleine
     * et que le name donnée n'est pas vide
     * @param name Le nom du student à rajouter à la promo, ne doit pas être vide
     * @return Un message de feedback selon si ça a marché ou pas
     */
    public String addStudent(String name) {
        if(name.isEmpty()) {
            return "Empty name is forbidden";
        }
        if(isFull()) {
            return "Promo is full";
        }

        students.add(name);
        return "The promo contains "+students.size()+" students";
    }
    /**
     * Boucle sur les students pour les afficher
     * @return Une String concaténant tous les students
     */
    public String display() {
        String listNames = "";
        for (String name : students) {
            listNames += name+"\n";
            
        }
        return listNames;
    }
    /**
     * Méthode qui renvoie true ou false selon si la promo est pleine ou pas
     * @return true si plein, false si pas plein
     */
    public boolean isFull() {
        return students.size() >= promoSize;
    }

    
}
